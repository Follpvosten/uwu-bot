use serde::Deserialize;

#[derive(Deserialize)]
pub struct BotConfig {
    pub token: String,
}

impl std::str::FromStr for BotConfig {
    type Err = toml::de::Error;
    fn from_str(s: &str) -> Result<BotConfig, Self::Err> {
        toml::from_str(s)
    }
}
