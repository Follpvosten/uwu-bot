use erased_serde::Serialize;
use futures::{future::Either, Future, IntoFuture, Stream};
use std::fs;

use telebot::{
    bot::{Bot, RequestHandle},
    functions::*,
    objects::*,
};

mod config;
use config::*;

mod uwu;
use uwu::uwuify;

fn handle_error(e: failure::Error) {
    #[cfg(debug_assertions)]
    panic!("Error: {}", e);
    #[cfg(not(debug_assertions))]
    eprintln!("Error: {}", e);
}

enum Request {
    Message(RequestHandle, Message, String),
    Inline(RequestHandle, InlineQuery),
}

fn main() {
    println!("Loading config.toml...");
    let bot_config: BotConfig = fs::read_to_string("config.toml")
        .expect("Failed to read config file!")
        .parse()
        .expect("Failed to parse config!");

    println!("Creating bot...");
    let bot = Bot::new(&bot_config.token).update_interval(200);

    loop {
        let stream = bot
            .clone()
            .get_stream(None)
            .filter_map(|(bot, mut update)| {
                if let Some(mut msg) = update.message.take() {
                    let text = msg.text.take()?;
                    if !text.starts_with("/uwu") {
                        return None;
                    }
                    Some(Request::Message(bot, msg, text))
                } else if let Some(iq) = update.inline_query.take() {
                    Some(Request::Inline(bot, iq))
                } else {
                    None
                }
            })
            .and_then(|request| match request {
                Request::Message(bot, msg, text) => {
                    let orig_text = if let Some(orig_msg) = msg.reply_to_message {
                        if let Some(txt) = orig_msg.text {
                            txt
                        } else {
                            text
                        }
                    } else {
                        text
                    };
                    let text = uwuify(&orig_text);
                    Either::A(bot.message(msg.chat.id, text).send().map(|_| (bot, true)))
                }
                Request::Inline(bot, iq) => {
                    use input_message_content::Text;
                    let text = uwuify(&iq.query);
                    let result: Vec<_> = [
                        "", "uwu", "uwu ~~~", "owo", "qwq", "ÜwÜ", "7w7", "ÔwÔ", "~wU", "~~", "ùwú",
                    ]
                    .iter()
                    .filter_map(|end| {
                        let title = if end.is_empty() {
                            if text.is_empty() {
                                return None;
                            }
                            "\"normal\"".into()
                        } else {
                            format!("ending with {}", end)
                        };
                        let text = format!("{} {}", text, end);
                        Some(Box::new(
                            InlineQueryResultArticle::new(title, Box::new(Text::new(text.clone())))
                                .description(text),
                        ) as Box<dyn Serialize + Send>)
                    })
                    .collect();
                    Either::B(bot.answer_inline_query(iq.id, result).send())
                }
            })
            .map_err(handle_error)
            .for_each(|_| Ok(()));

        println!("Starting loop...");
        tokio::run(stream.into_future());
    }
}
