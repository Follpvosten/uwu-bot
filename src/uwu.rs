use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
    static ref XD_REX: Regex = Regex::new(r"[xX]D").unwrap();
}

enum LanguageToken<'a> {
    Literal(&'a str),
    KnownWord(&'a str),
}

static KNOWN_WORDS: &[&str] = &["owo", "uwu", "lol"];

impl<'a> From<&'a str> for LanguageToken<'a> {
    fn from(s: &'a str) -> LanguageToken<'a> {
        if KNOWN_WORDS.contains(&s) {
            LanguageToken::KnownWord(s)
        } else {
            LanguageToken::Literal(s)
        }
    }
}

fn uwuify_simple(source: &str) -> String {
    source
        .replace(&['r', 'l'][..], "w")
        .replace(&['R', 'L'][..], "W")
        .replace("so", "sow")
        .replace(&XD_REX as &Regex, "owo")
        .replace("...", " uwu")
        .replace("ove", "uv")
        .replace("ou", "ew")
}

pub fn uwuify_complicated(source: &str) -> String {
    use LanguageToken::*;
    source
        .split("\n")
        .map(|line| {
            line.split_whitespace()
                .map(LanguageToken::from)
                .map(|token| match token {
                    Literal(l) => uwuify_simple(l),
                    KnownWord(w) => w.to_owned(),
                })
                .collect::<Box<[_]>>()
                .join(" ")
        })
        .collect::<Box<[_]>>()
        .join("\n")
}

pub fn uwuify(source: &str) -> String {
    uwuify_complicated(source)
}
